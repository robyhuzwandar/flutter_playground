package com.playground.app;

import android.os.Bundle;

import com.playground.app.R;

import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);
    String mystring = getResources().getString(R.string.token);
    System.out.println("INI STRING : " + mystring);
  }
}
