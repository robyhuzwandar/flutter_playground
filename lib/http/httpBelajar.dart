import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' show Client;

class BelajarHtpp extends StatefulWidget {
  @override
  _BelajarHtppState createState() => _BelajarHtppState();
}

class _BelajarHtppState extends State<BelajarHtpp> {
  Client client = Client();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: () {
          _onTap();
        },
        child: Text('Klik aku'),
      ),
    );
  }

  _onTap() {
    createLearnerAttempt('9513dd40-83d6-4982-8503-b99b54c16c41').then((res) {
      if (res) {
        print('START EXAMP READY');
      } else {
        print('START EXAMP GAGAL');
      }
    });
  }

  Future<bool> createLearnerAttempt(String exampId) async {
    String accessToken =
        'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI4X2ZGRl92Nml6S0xpTUhkTDVEcUZ5MkRHSjdsRkVtMlI3YW1oWmtPNk93In0.eyJqdGkiOiIwMWM4MzFkZi05MmEyLTQwODgtYTlhZS1lMzZhOGNjZjMwNTAiLCJleHAiOjE1Njc4MDUzNTUsIm5iZiI6MCwiaWF0IjoxNTY3NzY5MzU2LCJpc3MiOiJodHRwczovL2FjY291bnRzLmFwcC1kZXYuYWdvcmEuaWQvYXV0aC9yZWFsbXMvYWdvcmEiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiNDYxODgyNjItN2E2ZC00ZWRiLWFmOTctNmVkNTI4YWMxNzBiIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibW9iaWxlIiwiYXV0aF90aW1lIjoxNTY3NzY5MzU1LCJzZXNzaW9uX3N0YXRlIjoiNTc0OTllNGMtYjlmNC00M2E5LTlmNDItODA2ZmZjMmZiZTc1IiwiYWNyIjoiMSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsImxlYXJuZXIiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib3BlbmlkIGVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJIYXJpdHNhaCBCaW4gTnVhbSIsInByZWZlcnJlZF91c2VybmFtZSI6Im51YW0iLCJnaXZlbl9uYW1lIjoiSGFyaXRzYWgiLCJmYW1pbHlfbmFtZSI6IkJpbiBOdWFtIiwiZW1haWwiOiJoQG1haWwuY29tIn0.fL0PXkQH1KaCY78P2BmvoreSSpPpD-sJiI9-R5ko27oMmdRKPNl1HC_YjcN7_xwkR-0GAEd1g3eiXQmkOQVf4cwNP3RkEfN7wNEHFW7q67RqfJGM2HSh3IDIl16syaTm8m0nlhktQfnb6ezaEuAeBY3pdYQlsaQ5xCqju9qRRex5YnwVWLJ1gZ_0KSMKckMlqiHaqETCaBvJCDYvt0DPe2N7XnvYKCaRTsqdudXtYYYqD58BcgAEgKiwz3muGsYanv0kHKGZxQ34Y1fajLSKtkeOQBEm1UIuRNx1yuNEL0ldQKJ-CPU-ZL4X_HPHnl9VfK6vRSkVFKDP3gk_Ayf0oQ';
    print(accessToken);
    final response = await client.post(
        "https://exam.api-dev.agora.id/learner/exam/$exampId/attempt",
        headers: {HttpHeaders.authorizationHeader: "Basic $accessToken"});

    if (response.statusCode == 201) {
      print(response.body);
      return true;
    } else {
      return false;
    }
  }
}
