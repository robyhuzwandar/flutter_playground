class DataMock{
  final String lessonId;

  DataMock(
    {this.lessonId}
  );
}


List<DataMock> data = [
  DataMock(
    lessonId: "f3488686-2f5e-4f60-a7d7-1ecb6dec7e65"
  ),
  DataMock(
    lessonId: "4dbd97d1-15ae-4d07-a482-231ea12d027b"
  ),
  DataMock(
    lessonId: "32a592b9-9931-42ac-b470-74e6c39b02d1"
  ),
];

class Data2Mock{
  final String lessonId;

  Data2Mock(
    {this.lessonId}
  );
}


List<Data2Mock> data2 = [
  Data2Mock(
    lessonId: "f3488686-2f5e-4f60-a7d7-1ecb6dec7e65x"
  ),
  Data2Mock(
    lessonId: "4dbd97d1-15ae-4d07-a482-231ea12d027bx"
  ),
  Data2Mock(
    lessonId: "32a592b9-9931-42ac-b470-74e6c39b02d1"
  ),
];
