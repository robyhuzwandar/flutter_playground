import 'package:flutter/material.dart';
import 'package:playground_project/video/custom_chewie.dart';

class VideoWIdget extends StatefulWidget {
  @override
  _VideoWIdgetState createState() => _VideoWIdgetState();
}

class _VideoWIdgetState extends State<VideoWIdget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Center(
        child: ChewieDemo(),
      ),
    ));
  }
}
