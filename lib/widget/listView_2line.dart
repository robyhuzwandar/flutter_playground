import 'package:flutter/material.dart';
import 'package:playground_project/models/categoryModel.dart';
import 'package:playground_project/utils/mapping.dart';

class ListView2 extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<ListView2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  height: 50,
                  child: Row(
                    children: mapping(categories, (index, category) {
                      return index <= 5 ? _item(category, index) : Container();
                    }),
                  )),
              Container(
                  height: 50,
                  child: Row(
                    children: mapping(categories, (index, category) {
                      return index >= 6 && index <= 10
                          ? _item(category, index)
                          : Container();
                    }),
                  )),
            ],
          )
        ],
      ),
    ));
  }

  Widget _item(category, index) {
    return Container(
      padding: EdgeInsets.only(
          left: index == 0 ? 20.0 : 7.0, right: 7.0, top: 2.0, bottom: 2.0),
      child: InkWell(
        onTap: () {},
        child: Container(
          decoration: BoxDecoration(
              color: Colors.grey, borderRadius: BorderRadius.circular(8.0)),
          // margin: EdgeInsets.all(0.0),
          child: Padding(
            padding: EdgeInsetsDirectional.only(end: 10.0),
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Text(category._title,
                  style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                  )),
            ),
          ),
        ),
      ),
    );
  }
}
