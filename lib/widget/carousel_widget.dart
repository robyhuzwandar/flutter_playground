import 'package:flutter/material.dart';
import 'package:carousel_widget/src/carousel/components/bar_point_selected.dart';
import 'package:carousel_widget/src/carousel/components/point_selected.dart';

class MyCarousel extends StatelessWidget {
  final List<Widget> listViews = [
    Container(
        width: 300.0,
        child: Image.network(
            'https://cdn-images-1.medium.com/max/2000/1*GqdzzfB_BHorv7V2NV7Jgg.jpeg')),
    Container(
        width: 300.0,
        child: Image.network(
            'https://cdn-images-1.medium.com/max/2000/1*GqdzzfB_BHorv7V2NV7Jgg.jpeg')),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CarouselController(listViews: listViews),
    );
  }
}

class CarouselController extends StatefulWidget {
  final List<dynamic> listViews;
  CarouselController({
    @required this.listViews,
  });
  @override
  State<StatefulWidget> createState() {
    return _CarouselController();
  }
}

class _CarouselController extends State<CarouselController> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              bottom: (MediaQuery.of(context).size.height / 12),
            ),
            child: PageView(
              onPageChanged: (index) => setState(() => _currentIndex = index),
              physics: BouncingScrollPhysics(),
              children: widget.listViews,
            ),
          ),
          BarPointSelected(
              currentIndex: _currentIndex,
              listViews: widget.listViews
                  .map<Widget>((pointer) => PointSelected(
                        currentIndex: _currentIndex,
                        position: widget.listViews.indexOf(pointer),
                      ))
                  .toList())
        ],
      ),
    );
  }
}
